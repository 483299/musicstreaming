﻿using DataAccessLayer;
using InterfaceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicStreamingUnitTests
{
    public class SongStubDAL : ISongDAL
    {
        private Dictionary<int, SongDTO> data = new Dictionary<int, SongDTO>();
        private SongArtistStubDAL songArtistStubDAL;

        public SongStubDAL(SongArtistStubDAL songArtistStubDAL)
        {
            this.songArtistStubDAL = songArtistStubDAL;
        }

        public void DeleteSongByID(int id)
        {
            data.Remove(id);
        }

        public SongDTO GetSongByID(int id)
        {
            data.TryGetValue(id, out SongDTO song);
            return song;
        }

        public void InsertSong(SongDTO dto)
        {
            if (data.ContainsKey(dto.ID)) return;
            data.Add(dto.ID, dto);
        }

        public void InsertSong(SongDTO dto, int artistId)
        {
            if (data.ContainsKey(dto.ID)) return;
            data.Add(dto.ID, dto);

            songArtistStubDAL.LinkArtistToSong(artistId, dto);
        }

        public List<SongDTO> RetrieveSongs()
        {
            return data.Values.ToList();
        }

        public void UpdateSong(SongDTO dto)
        {
            data[dto.ID] = dto;
        }

        public List<ArtistDTO> GetArtistsBySong(int songId)
        {
            return songArtistStubDAL.GetArtistDTOs(songId);
        }

        public void LinkSongToArtist(int songId, ArtistDTO artistDTO)
        {
            songArtistStubDAL.LinkSongToArtist(songId, artistDTO);
        }
    }
}
