using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using InterfaceLayer;
using LogicLayer;
using System.Collections.Generic;

namespace MusicStreamingUnitTests
{
    [TestClass]
    public class SongDALUnitTests
    {
        private SongArtistStubDAL? songArtistStubDAL;
        private SongContainer? songContainer;
        private SongStubDAL? songStubDAL;
        private ArtistContainer? artistContainer;
        private ArtistStubDAL? artistStubDAL;

        [TestInitialize]
        public void Init()
        {
            songArtistStubDAL = new SongArtistStubDAL();
            songStubDAL = new SongStubDAL(songArtistStubDAL);
            artistStubDAL = new ArtistStubDAL(songArtistStubDAL);
            songContainer = new SongContainer(songStubDAL);
            artistContainer = new ArtistContainer(artistStubDAL);
        }

        [TestMethod]
        public void TestSongDALInsert()
        {
            //Arrange

            Song song1 = new Song(
                1,
                "Test",
                15
                );

            Song song2 = new Song(
                2,
                "Test2",
                18
                );

            Song song3 = new Song(
                3,
                "Test",
                19
                );

            //Act

            songContainer.InsertSong(song1);

            songContainer.InsertSong(song2);

            songContainer.InsertSong(song3);

            Song s = songContainer.GetSongByID(1);
            //Assert

            Assert.AreEqual(songStubDAL.GetSongByID(1), song1.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(2), song2.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(3), song3.GetDTO());
        }

        [TestMethod]
        public void TestSongDALDuplicateInsert()
        {
            //Arrange
            Song song1 = new Song(
                1,
                "Test",
                15
                );

            Song song2 = new Song(
                1,
                "Test2",
                18
                );

            //Act

            songContainer.InsertSong(song1);
            songContainer.InsertSong(song2);


            //Assert

            Assert.AreEqual(songStubDAL.GetSongByID(1), song1.GetDTO());
            Assert.AreNotEqual(songStubDAL.GetSongByID(1), song2.GetDTO());
        }

        [TestMethod]
        public void TestSongDALUpdate()
        {
            //Arrange
            Song song = new Song(
                1,
                "Test",
                15);

            //Act

            songContainer.InsertSong(song);

            Song songUpdated = new Song(1, "Test", 20);
            songContainer.UpdateSong(songUpdated);

            //Assert

            Assert.IsFalse(songStubDAL.GetSongByID(1).Length == song.Length);
        }


        [TestMethod]
        public void TestDeleteSong()
        {
            //Arrange

            Song song = new Song(
                1,
                "Test",
                15);

            //Act

            songContainer.InsertSong(song);

            songContainer.DeleteSongByID(song.ID);


            //Assert

            Assert.IsTrue(songStubDAL.GetSongByID(1).ID != song.ID);

        }

        [TestMethod]
        public void TestMultipleInsertSong()
        {

            //Arrange
            Song song = new Song(1, "Test", 15);
            Song song1 = new Song(10, "TEST", 20);
            Song song2 = new Song(500, "TEst", 5006);
            Song song3 = new Song(01020, "bruh", 465046);
            Song song4 = new Song(4174832, "oei", 5357305);
            Song song5 = new Song(35345, "jaja", 2489024);
            Song song6 = new Song(4242, "dfgdg", 535345);
            Song song7 = new Song(53534, "efsfg", 366456);
            Song song8 = new Song(5345345, "fewfsfd", 435636);


            //Act
            songContainer.InsertSong(song);
            songContainer.InsertSong(song1);
            songContainer.InsertSong(song2);
            songContainer.InsertSong(song3);
            songContainer.InsertSong(song4);
            songContainer.InsertSong(song5);
            songContainer.InsertSong(song6);
            songContainer.InsertSong(song7);
            songContainer.InsertSong(song8);

            //Assert
            Assert.AreEqual(songStubDAL.GetSongByID(1), song.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(10), song1.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(500), song2.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(01020), song3.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(4174832), song4.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(35345), song5.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(4242), song6.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(53534), song7.GetDTO());
            Assert.AreEqual(songStubDAL.GetSongByID(5345345), song8.GetDTO());
        }


        [TestMethod]
        public void TestGetSongs()
        {
            //Arrange
            Song song = new Song(1, "Test", 15);
            Song song1 = new Song(10, "TEST", 20);
            Song song2 = new Song(500, "TEst", 5006);
            Song song3 = new Song(01020, "bruh", 465046);
            Song song4 = new Song(4174832, "oei", 5357305);
            Song song5 = new Song(35345, "jaja", 2489024);
            Song song6 = new Song(4242, "dfgdg", 535345);
            Song song7 = new Song(53534, "efsfg", 366456);
            Song song8 = new Song(5345345, "fewfsfd", 435636);


            //Act
            songContainer.InsertSong(song);
            songContainer.InsertSong(song1);
            songContainer.InsertSong(song2);
            songContainer.InsertSong(song3);
            songContainer.InsertSong(song4);
            songContainer.InsertSong(song5);
            songContainer.InsertSong(song6);
            songContainer.InsertSong(song7);
            songContainer.InsertSong(song8);

            List<SongDTO> songs = songStubDAL.RetrieveSongs();

            //Assert
            Assert.AreEqual(songs[0], song.GetDTO());
            Assert.AreEqual(songs[1], song1.GetDTO());
            Assert.AreEqual(songs[2], song2.GetDTO());
            Assert.AreEqual(songs[3], song3.GetDTO());
            Assert.AreEqual(songs[4], song4.GetDTO());
            Assert.AreEqual(songs[5], song5.GetDTO());
            Assert.AreEqual(songs[6], song6.GetDTO());
            Assert.AreEqual(songs[7], song7.GetDTO());
            Assert.AreEqual(songs[8], song8.GetDTO());

        }

        [TestMethod]
        public void TestGetArtistBySong()
        {
            //Arrange
            Song song = new Song(1, "Test", 20);
            Artist artist = new Artist(1, "kanye", 20);
         
            //Act
            songContainer.InsertSong(song);
            artistContainer.InsertArtist(artist);
            songContainer.LinkSongToArtist(song.ID, artist);

            List<ArtistDTO> artists = songStubDAL.GetArtistsBySong(song.ID);

            //Assert
            Assert.AreEqual(artists[0], artist.GetDTO());
        }
    }
}