﻿using InterfaceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicStreamingUnitTests
{
    public class ArtistStubDAL : IArtistDAL
    {
        private Dictionary<int, ArtistDTO> data = new Dictionary<int, ArtistDTO>();
        private SongArtistStubDAL songArtistStubDAL;

        public ArtistStubDAL(SongArtistStubDAL songArtistStubDAL)
        {
            this.songArtistStubDAL = songArtistStubDAL;
        }

        public ArtistDTO FindArtistByID(int ArtistID)
        {
            data.TryGetValue(ArtistID, out ArtistDTO result);
            return result;
        }

        public List<SongDTO> GetSongsByArtist(int ArtistID)
        {

            return songArtistStubDAL.GetSongDTOs(ArtistID);
        }

        public void InsertArtist(ArtistDTO artistDTO)
        {
            if (data.ContainsKey(artistDTO.ID)) return;
            data.Add(artistDTO.ID, artistDTO);
        }

        public List<ArtistDTO> LoadArtist()
        {
            return data.Values.ToList();
        }

        public void UpdateArtist(ArtistDTO dto)
        {
            data[dto.ID] = dto;
        }

        public List<ArtistDTO> GetSongsbyArtist(int SongID)
        {
            return songArtistStubDAL.GetArtistDTOs(SongID);
        }

        public void LinkArtistToSong(int artistId, SongDTO songDto)
        {
            songArtistStubDAL.LinkArtistToSong(artistId, songDto);
        }
    }
}
