using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using InterfaceLayer;
using LogicLayer;
using System.Collections.Generic;

namespace MusicStreamingUnitTests
{
    [TestClass]
    public class ArtistDALUnitTests
    {
        private SongArtistStubDAL? songArtistStubDAL;
        private SongContainer? songContainer;
        private SongStubDAL? songStubDAL;
        private ArtistContainer? artistContainer;
        private ArtistStubDAL? artistStubDAL;

        [TestInitialize]
        public void Init()
        {
            songArtistStubDAL = new SongArtistStubDAL();
            songStubDAL = new SongStubDAL(songArtistStubDAL);
            artistStubDAL = new ArtistStubDAL(songArtistStubDAL);
            songContainer = new SongContainer(songStubDAL);
            artistContainer = new ArtistContainer(artistStubDAL);
        }

        [TestMethod]
        public void TestArtistDalInsert()
        {
            //Arrange

            Artist artist1 = new Artist(
                1,
                "Test",
                15
                );

            Artist artist2 = new Artist(
                2,
                "Test2",
                18
                );

            Artist artist3 = new Artist(
                3,
                "Test",
                19
                );

            //Act

            artistContainer.InsertArtist(artist1);

            artistContainer.InsertArtist(artist2);

            artistContainer.InsertArtist(artist3);

            //Assert

            Assert.AreEqual(artistStubDAL.FindArtistByID(1), artist1.GetDTO());
            Assert.AreEqual(artistStubDAL.FindArtistByID(2), artist2.GetDTO());
            Assert.AreEqual(artistStubDAL.FindArtistByID(3), artist3.GetDTO());
        }

        [TestMethod]
        public void TestArtistDALDuplicateInsert()
        {
            //Arrange
            Artist artist1 = new Artist(
                1,
                "Test",
                15
                );

            Artist artist2 = new Artist(
                1,
                "Test2",
                18
                );

            //Act

            artistContainer.InsertArtist(artist1);
            artistContainer.InsertArtist(artist2);

            //Assert

            Assert.AreEqual(artistStubDAL.FindArtistByID(1), artist1.GetDTO());
            Assert.AreNotEqual(artistStubDAL.FindArtistByID(1), artist2.GetDTO());
        }

        [TestMethod]
        public void TestArtistDALUpdate()
        {
            //Arrange
            Artist artist = new Artist(
                1,
                "Test",
                15);

            //Act

            artistContainer.InsertArtist(artist);

            Artist artistUpdated = new Artist(1, "Test", 20);
            artistContainer.UpdateArtist(artistUpdated);

            //Assert

            Assert.IsFalse(artistStubDAL.FindArtistByID(1).Age == artist.Age);
        }

        [TestMethod]
        public void TestArtistDALUpdateIncorrectID()
        {
            //Arrange
            Artist artist = new Artist(
                1,
                "Test",
                15);

            //Act

            artistContainer.InsertArtist(artist);

            Artist artistUpdated = new Artist(2, "Test", 20);
            artistContainer.UpdateArtist(artistUpdated);

            //Assert

            Assert.IsTrue(artistStubDAL.FindArtistByID(1).Age == artist.Age);
        }

        [TestMethod]
        public void TestArtistsBySong()
        {
            //Arrange
            Artist artist = new Artist(
                1,
                "Test",
                42);

            Song song = new Song(1, "Test", 20);

            //Act
            artistContainer.InsertArtist(artist);
            songContainer.InsertSong(song);
            songContainer.LinkSongToArtist(song.ID, artist);

            List<ArtistDTO> artists = songStubDAL.GetArtistsBySong(song.ID);

            //Assert
            Assert.AreEqual(artists[0], artist.GetDTO());

        }

        [TestMethod]
        public void TestUpdateArtist()
        {
            //Arrange 
            Artist artist = new Artist(
                1,
                "Test",
                45);

            Artist updatedArtist = new Artist(1, "Test", 20);

            //Act
            artistContainer.InsertArtist(artist);

            artistContainer.UpdateArtist(updatedArtist);

            //Assert
            Assert.AreEqual(artistStubDAL.FindArtistByID(1), updatedArtist.GetDTO());
        }

        [TestMethod]
        public void TestSongbyArtist()
        {
            Song song = new Song(1, "Test", 20);
            Artist artist = new Artist(2, "Test", 20);


            //Act
            artistContainer.InsertArtist(artist);
            songContainer.InsertSong(song);
            artistContainer.LinkArtistToSong(artist.ID, song);

            List<SongDTO> songs = artistStubDAL.GetSongsByArtist(artist.ID);

            //Assert
            Assert.AreEqual(songs[0], song.GetDTO());
        }

        [TestMethod]
        public void TestArtistSearchByID()
        {
            //Arrange            
            Artist artist = new Artist(2, "Test", 20);

            //Act
            artistContainer.InsertArtist(artist);

            //Assert
            Assert.AreEqual(artistStubDAL.FindArtistByID(artist.ID), artist.GetDTO());

        }
    }
}