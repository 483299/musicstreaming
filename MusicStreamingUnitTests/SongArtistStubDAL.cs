﻿using InterfaceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicStreamingUnitTests
{
    public class SongArtistStubDAL
    {
        public Dictionary<int, List<ArtistDTO>> songArtistPairs = new Dictionary<int, List<ArtistDTO>>();
        public Dictionary<int, List<SongDTO>> artistSongPairs = new Dictionary<int, List<SongDTO>>();

        public List<ArtistDTO> GetArtistDTOs(int songId)
        {
            songArtistPairs.TryGetValue(songId, out List<ArtistDTO>? artists);
            if(artists == null) return new List<ArtistDTO>();
            return artists;
        }

        public List<SongDTO> GetSongDTOs(int artistId)
        {
            artistSongPairs.TryGetValue(artistId, out List<SongDTO>? songs);
            if (songs == null) return new List<SongDTO>();
            return songs;
        }

        public void LinkArtistToSong(int artistId, SongDTO songDTO)
        {
            artistSongPairs.TryGetValue(artistId, out List<SongDTO>? songDTOs);
            if (songDTOs == null) songDTOs = new List<SongDTO>();
            songDTOs.Add(songDTO);
            artistSongPairs.Add(artistId, songDTOs);
        }

        public void LinkSongToArtist(int songId, ArtistDTO artistDTO)
        {
            songArtistPairs.TryGetValue(songId, out List<ArtistDTO>? artistDTOs);
            if (artistDTOs == null) artistDTOs = new List<ArtistDTO>();
            artistDTOs.Add(artistDTO);
            songArtistPairs.Add(songId, artistDTOs);
        }
    }
}
