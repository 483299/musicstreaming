﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using InterfaceLayer;

namespace DataAccessLayer
{
    public class SongDAL : ISongDAL
    {
        private Database database;

        public SongDAL()
        {
            database = new Database();
        }

        public void InsertSong(SongDTO dto)
        {
            string query = "INSERT into Song (Length, Name) VALUES (@Length, @Name)";
            SqlParameter sqlIDParam = new SqlParameter("@ID", SqlDbType.Int);
            sqlIDParam.Value = Convert.ToInt32(dto.ID);
            SqlParameter sqlAgeParam = new SqlParameter("@Length", SqlDbType.Int);
            sqlAgeParam.Value = Convert.ToInt32(dto.Length);
            SqlParameter sqlNameParam = new SqlParameter("@Name", SqlDbType.Text);
            sqlNameParam.Value = dto.Name;

            using (SqlConnection connection = database.GetConnection())
            {

                using (SqlCommand sql = new SqlCommand(query, connection))
                {
                    sql.Parameters.Add(sqlNameParam);
                    sql.Parameters.Add(sqlAgeParam);
                    sql.Parameters.Add(sqlIDParam);

                    try
                    {
                        connection.Open();
                        sql.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

        }
        
        public SongDTO GetSongByID(int id)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string cmd = "SELECT * FROM Song WHERE Song.id = @ID";

                SqlParameter sqlIDParam = new SqlParameter("@ID", SqlDbType.Int);
                sqlIDParam.Value = id;

                using (SqlCommand sql = new SqlCommand(cmd, connection))
                {
                    sql.Parameters.Add(sqlIDParam);

                    sql.ExecuteNonQuery();

                    using (SqlDataReader reader = sql.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SongDTO song = new SongDTO();
                            song.ID = (int)reader["ID"];
                            song.Length = (int)reader["Length"];
                            song.Name = (string)reader["Name"];
                            return song;
                        }
                    }
                }
            }

            return new SongDTO();
        }

        public void DeleteSongByID(int id)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string query = $"DELETE FROM Song WHERE ID = @ID";
                SqlCommand sql = new SqlCommand(query, connection);
                sql.Parameters.Add(new SqlParameter("@ID", id));
                sql.ExecuteNonQuery();
                connection.Close();                
            }
        }

        public List<SongDTO> RetrieveSongs()
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                SqlCommand cmd1 = new SqlCommand("SELECT * FROM Song", connection);
                cmd1.ExecuteNonQuery();

                List<SongDTO> list = new List<SongDTO>();
                using (SqlDataReader reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        SongDTO song = new SongDTO();
                        song.ID = (int)reader["ID"];
                        song.Length = (int)reader["Length"];
                        song.Name = (string)reader["Name"];
                        list.Add(song);
                    }
                }
                connection.Close();
                return list;
            }
        }

        public void UpdateSong(SongDTO dto)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string cmd = "UPDATE Song SET Song.Name=@Name, Song.Length=@Length WHERE Song.id=@ID";

                SqlParameter sqlIDParam = new SqlParameter("@ID", SqlDbType.Int);
                sqlIDParam.Value = dto.ID;

                SqlParameter sqlNameParam = new SqlParameter("@Name", SqlDbType.Text);
                sqlNameParam.Value = dto.Name;

                SqlParameter sqlLengthParam = new SqlParameter("@Length", SqlDbType.Int);
                sqlLengthParam.Value = dto.Length;

                using (SqlCommand sql = new SqlCommand(cmd, connection))
                {
                    sql.Parameters.Add(sqlIDParam);
                    sql.Parameters.Add(sqlNameParam);
                    sql.Parameters.Add(sqlLengthParam);

                    sql.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        public List<ArtistDTO> GetArtistsBySong(int songId)
        {
            List<ArtistDTO> Result = new List<ArtistDTO>();

            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string query = "SELECT * FROM ArtistSongCollection c INNER JOIN Artist a ON a.ID = c.ArtistID WHERE c.SongID = @SongID";
                SqlParameter sqlSongIDparam = new SqlParameter("@SongID", SqlDbType.Int);
                sqlSongIDparam.Value = songId;

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.Add(sqlSongIDparam);
                    SqlDataReader dr;
                    dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        Result.Add(new ArtistDTO { ID = (int)dr["ID"], Name = (string)dr["Name"], Age = (int)dr["Age"] });
                    }
                    dr.Close();
                    cmd.Dispose();
                    connection.Close();

                    return Result;
                }
            }

        }
        public void LinkSongToArtist(int songId, ArtistDTO artistDTO)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string query = "INSERT INTO ArtistSongCollection(ArtistID, SongID) VALUES (@ArtistID, @SongID)";

                SqlParameter sqlArtistParam = new SqlParameter("@ArtistID", SqlDbType.Int);
                sqlArtistParam.Value = Convert.ToInt32(artistDTO.ID);
                SqlParameter sqlSongParam = new SqlParameter("@SongID", SqlDbType.Int);
                sqlSongParam.Value = songId;

                using (SqlCommand sql = new SqlCommand(query, connection))
                {
                    sql.Parameters.Add(sqlArtistParam);
                    sql.Parameters.Add(sqlSongParam);

                    sql.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
    }
}
