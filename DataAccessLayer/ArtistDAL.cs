﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using InterfaceLayer;

namespace DataAccessLayer
{
    public class ArtistDAL : IArtistDAL 
    {
        private Database database;

        public ArtistDAL()
        {
            database = new Database();
        }

        public void InsertArtist(ArtistDTO dto)
        {
            string query = "INSERT into Artist (Name, Age) VALUES (@Name, @Age)";
            SqlParameter sqlnameParam = new SqlParameter("@Name", SqlDbType.Text);
            sqlnameParam.Value = dto.Name;
            SqlParameter sqlageParam = new SqlParameter("@Age", SqlDbType.Int);
            sqlageParam.Value = Convert.ToInt32(dto.Age);

            using (SqlConnection connection = database.GetConnection())
            {
                using (SqlCommand sql = new SqlCommand(query, connection))
                {
                    sql.Parameters.Add(sqlnameParam);
                    sql.Parameters.Add(sqlageParam);

                    try
                    {
                        connection.Open();
                        sql.ExecuteNonQuery();
                    } 
                    finally
                    {
                        connection.Close();
                    }
                        
                }
            }
        }    
        
        public List<ArtistDTO> LoadArtist()
        {
            List<ArtistDTO> Result = new List<ArtistDTO>();

            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string cmd1 = "SELECT * FROM Artist";
                SqlCommand cmd = new SqlCommand(cmd1, connection);
                SqlDataReader dr;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Result.Add(new ArtistDTO { ID = (int)dr["ID"], Name = (string)dr["Name"], Age = (int)dr["Age"]});
                }

                dr.Close();
                cmd.Dispose();
                connection.Close();

                return Result;
            }
        }

        public List<SongDTO> GetSongsByArtist(int ArtistID)
        {
            List<SongDTO> Result = new List<SongDTO>();

            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string cmd1 = "SELECT * FROM ArtistSongCollection a INNER JOIN Song s ON s.ID = a.SongID WHERE a.ArtistID = @ArtistID";
                SqlParameter sqlArtistIDparam = new SqlParameter("@ArtistID", SqlDbType.Int);
                sqlArtistIDparam.Value = ArtistID;

                using (SqlCommand cmd = new SqlCommand(cmd1, connection))
                {
                    cmd.Parameters.Add(sqlArtistIDparam);
                    SqlDataReader dr;
                    dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        Result.Add(new SongDTO { ID = (int)dr["ID"], Name = (string)dr["Name"], Length = (int)dr["Length"] });
                    }
                    dr.Close();
                    cmd.Dispose();
                    connection.Close();

                    return Result;
                }
            }

        }

        public ArtistDTO FindArtistByID(int ArtistID)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string cmd = "SELECT * FROM Artist WHERE Artist.ID = @ID";

                SqlParameter sqlIDParam = new SqlParameter("@ID", SqlDbType.Int);
                sqlIDParam.Value = ArtistID;

                using (SqlCommand sql = new SqlCommand(cmd, connection))
                {
                    sql.Parameters.Add(sqlIDParam);

                    sql.ExecuteNonQuery();

                    using (SqlDataReader reader = sql.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ArtistDTO artist = new ArtistDTO();
                            artist.ID = (int)reader["ID"];
                            artist.Name = (string)reader["Name"];
                            artist.Age = (int)reader["Age"];
                            return artist;
                        }
                    }
                }
            }
            return new ArtistDTO();
        }

        public void UpdateArtist(ArtistDTO dto)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string cmd = "UPDATE Artist SET Artist.Name=@Name, Artist.Age=@Length WHERE Artist.ID=@ID";

                SqlParameter sqlIDParam = new SqlParameter("@ID", SqlDbType.Int);
                sqlIDParam.Value = dto.ID;

                SqlParameter sqlNameParam = new SqlParameter("@Name", SqlDbType.Text);
                sqlNameParam.Value = dto.Name;

                SqlParameter sqlAgeParam = new SqlParameter("@Age", SqlDbType.Int);
                sqlAgeParam.Value = dto.Age;

                using (SqlCommand sql = new SqlCommand(cmd, connection))
                {
                    sql.Parameters.Add(sqlIDParam);
                    sql.Parameters.Add(sqlNameParam);
                    sql.Parameters.Add(sqlAgeParam);

                    sql.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        public void LinkArtistToSong(int artistId, SongDTO songDTO)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string query = "INSERT INTO ArtistSongCollection(ArtistID, SongID) VALUES (@ArtistID, @SongID)";

                SqlParameter sqlArtistParam = new SqlParameter("@ArtistID", SqlDbType.Int);
                sqlArtistParam.Value = artistId;
                SqlParameter sqlSongParam = new SqlParameter("@SongID", SqlDbType.Int);
                sqlSongParam.Value = Convert.ToInt32(songDTO.ID);

                    using (SqlCommand sql = new SqlCommand(query, connection))
                    {
                        sql.Parameters.Add(sqlArtistParam);
                        sql.Parameters.Add(sqlSongParam);

                        try
                        {
                            connection.Open();
                            sql.ExecuteNonQuery();
                        }
                        finally
                        {
                            connection.Close();
                        }

                    }
                }
            }
        public void UnlinkSongFromArtist(int artistId, int songId)
        {
            using (SqlConnection connection = database.GetConnection())
            {
                connection.Open();
                string query = $"DELETE FROM ArtistSongCollection WHERE ArtistID = @ArtistID AND SongID = @SongID";
                SqlCommand sql = new SqlCommand(query, connection);
                sql.Parameters.Add(new SqlParameter("@ArtistID", artistId));
                sql.Parameters.Add(new SqlParameter("@SongID", songId));
                sql.ExecuteNonQuery();
                connection.Close();
            }
        }
    }    
}             
