﻿using InterfaceLayer;

namespace LogicLayer
{
    
    public class Song
    {
        public int ID { get; private set; }
        public string Name { get; private set; }
        public int Length { private set; get; }

        public SongDTO GetDTO()
        {
            return new SongDTO(ID, Name, Length);
        }
        
        public Song(SongDTO DTO)
        {
            ID = DTO.ID;
            Name = DTO.Name;
            Length = DTO.Length;
        }

        public Song(string name, int length)
        {
            this.Name = name;
            this.Length = length;
        }

        public Song(int ID, string name, int length)
        {
            this.ID = ID;
            this.Name = name;
            this.Length = length;
        }

        public override bool Equals(object? s)
        {
            if (s is Song) 
            {
                Song song = (Song)s;
                return song.ID == ID && song.Name == Name && song.Length == Length;
            } else if(s is SongDTO)
            {
                SongDTO song = (SongDTO)s;
                return song.ID == ID && song.Name == Name && song.Length == Length;
            }

            return false;
        }
    }
   
}