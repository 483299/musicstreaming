﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using InterfaceLayer;

namespace LogicLayer
{
    public class ArtistContainer : IArtistContainer
    {
        public List<Artist> artist = new List<Artist>();
        private IArtistDAL IArtistDAL;

        public ArtistContainer(IArtistDAL IArtistDAL)
        {
            this.IArtistDAL = IArtistDAL;
        }

        public void InsertArtist(Artist artist)
        {
            IArtistDAL.InsertArtist(artist.GetDTO());
        }

        public void LinkArtistToSong(int artistId, Song song)
        {
            IArtistDAL.LinkArtistToSong(artistId, new SongDTO(song.ID, song.Name, song.Length));
        }

        public List<Artist> LoadArtist()
        {
            List<ArtistDTO> artistDtos = IArtistDAL.LoadArtist();
            List<Artist> artists = new List<Artist>();
            foreach (ArtistDTO artist in artistDtos)
            {
                artists.Add(new Artist(artist));
            }
            return artists;
        }

        public void UpdateArtist(Artist artist)
        {
            IArtistDAL.UpdateArtist(new ArtistDTO(artist.ID, artist.Name, artist.Age));
        }

        public Artist FindArtistByID(int ArtistID)
        {
            return (new Artist(IArtistDAL.FindArtistByID(ArtistID)));
        }

        public List<Song> GetSongsByArtist(int artistID)
        {
            List<Song> songs = new List<Song>();
            foreach (SongDTO songDTO in IArtistDAL.GetSongsByArtist(artistID))
            {
                songs.Add(new Song(songDTO));
            }

            return songs;
        }

        public void UnlinkArtistFromSong(int artistId, int songId)
        {
            IArtistDAL.UnlinkSongFromArtist(artistId, songId);
        }
    }
}
