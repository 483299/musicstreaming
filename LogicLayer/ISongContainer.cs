﻿
namespace LogicLayer
{
    public interface ISongContainer
    {
        void DeleteSongByID(int id);
        List<Artist> GetArtistsBySong(int songId);
        Song GetSongByID(int id);
        List<Song> GetSongs();
        void InsertSong(Song song);
        void LinkSongToArtist(int songId, Artist artist);
        void UpdateSong(Song song);
    }
}