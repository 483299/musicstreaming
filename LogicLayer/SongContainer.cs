﻿using InterfaceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer
{
    public class SongContainer : ISongContainer
    {
        private ISongDAL ISongDAL;

        public SongContainer(ISongDAL ISongDAL)
        {
            this.ISongDAL = ISongDAL;
        }

        public Song GetSongByID(int id)
        {
            return new Song(ISongDAL.GetSongByID(id));
        }

        public List<Song> GetSongs()
        {
            List<SongDTO> songDtos = ISongDAL.RetrieveSongs();
            List<Song> songs = new List<Song>();
            for (int i = 0; i < songDtos.Count; i++)
            {
                songs.Add(new Song(songDtos[i]));
            }
            return songs;
        }

        public void UpdateSong(Song song)
        {
            ISongDAL.UpdateSong(song.GetDTO());
        }

        public void DeleteSongByID(int id)
        {
            ISongDAL.DeleteSongByID(id);
        }

        public void InsertSong(Song song)
        {
            ISongDAL.InsertSong(song.GetDTO());
        }

        public void LinkSongToArtist(int songId, Artist artist)
        {
            ISongDAL.LinkSongToArtist(songId, new ArtistDTO(artist.ID, artist.Name, artist.Age));
        }

        public List<Artist> GetArtistsBySong(int songId)
        {
            List<Artist> artists = new List<Artist>();
            foreach (ArtistDTO artistDTO in ISongDAL.GetArtistsBySong(songId))
            {
                artists.Add(new Artist(artistDTO));
            }

            return artists;
        }

        public void LinkSongToArtist(int songId, ArtistDTO artistDTO)
        {
            ISongDAL.LinkSongToArtist(songId, artistDTO);
        }
    }
}
