﻿
namespace LogicLayer
{
    public interface IArtistContainer
    {
        Artist FindArtistByID(int ArtistID);
        List<Song> GetSongsByArtist(int artistID);
        void InsertArtist(Artist artist);
        void LinkArtistToSong(int artistId, Song song);
        List<Artist> LoadArtist();
        void UpdateArtist(Artist artist);
        void UnlinkArtistFromSong(int artistId, int songId);
    }
}