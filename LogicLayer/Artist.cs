﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using InterfaceLayer;

namespace LogicLayer
{
    public class Artist
    {
        public int ID { get; private set; }
        public string Name { get; private set; }
        public int Age { get; private set; }

        public ArtistDTO GetDTO()
        {
            return new ArtistDTO(ID, Name, Age);
        }

        public Artist(ArtistDTO DTO)
        {
            ID = DTO.ID;
            Name = DTO.Name;
            Age = DTO.Age;
        }

        public Artist(string Name, int Age)
        {
            this.ID = ID;
            this.Name = Name;
            this.Age = Age;
        }

        public Artist(int ID, string Name, int Age)
        {
            this.ID = ID;
            this.Name = Name;
            this.Age = Age;
        }

        public override bool Equals(object? a)
        {
            if (a is Artist)
            {
                Artist artist = (Artist)a;
                return artist.ID == ID && artist.Name == Name && artist.Age == Age;
            } else if(a is ArtistDTO)
            {
                ArtistDTO artist = (ArtistDTO)a;
                return artist.ID == ID && artist.Name == Name && artist.Age == Age;
            }

            return false;
        }
    }    
}
