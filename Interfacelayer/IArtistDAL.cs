﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace InterfaceLayer
{
    public interface IArtistDAL
    {
        public void InsertArtist(ArtistDTO artistDTO);
        public List<ArtistDTO> LoadArtist();
        public List<SongDTO> GetSongsByArtist(int ArtistID);
        public ArtistDTO FindArtistByID(int ArtistID);
        public void UpdateArtist(ArtistDTO dto);
        public void LinkArtistToSong(int artistId, SongDTO songDTO);
        public void UnlinkSongFromArtist(int artistId, int songId);
    }
}
