﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLayer
{
    public interface ISongDAL
    {
        public void InsertSong(SongDTO dto);
        public List<SongDTO> RetrieveSongs();
        public void DeleteSongByID(int id);
        public void UpdateSong(SongDTO dto);
        public SongDTO GetSongByID(int id);
        public List<ArtistDTO> GetArtistsBySong(int songId);
        public void LinkSongToArtist(int songId, ArtistDTO artistDTO);
    }
}
