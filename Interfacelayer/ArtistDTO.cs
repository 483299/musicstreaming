﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace InterfaceLayer
{
    public struct ArtistDTO
    {
        public int ID;
        public string Name;
        public int Age;

        public ArtistDTO(int ID, string Name, int Age)
        {
            this.ID = ID;
            this.Name = Name;
            this.Age = Age;
        }
    }

}
