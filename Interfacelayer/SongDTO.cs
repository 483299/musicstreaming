﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceLayer
{
    public struct SongDTO
    {
        public int ID;
        public string Name;
        public int Length;

        public SongDTO(int ID, string Name, int Length)
        {
            this.ID = ID;
            this.Name = Name;
            this.Length = Length;
        }
    }
}
