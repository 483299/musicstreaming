﻿using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Models;
using LogicLayer;
using DataAccessLayer;

namespace PresentationLayer.Controllers
{
    public class SongController : Controller
    {
        private readonly IArtistContainer artistContainer;
        private readonly ISongContainer songContainer;

        public SongController(IArtistContainer artistContainer, ISongContainer songContainer)
        {
            this.artistContainer = artistContainer;
            this.songContainer = songContainer;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult PlayMusic()
        {
            return View();
        }

        public IActionResult AddMusic()
        {
            return View();
        }

        public IActionResult ListMusic()
        {
            List<Song> songs = songContainer.GetSongs();
            List<SongViewModel> musicVMs = new List<SongViewModel>();

            foreach(Song s in songs)
            {
                musicVMs.Add(new SongViewModel(s.ID, s.Name, s.Length));
            }

            return View(musicVMs);
        }

        public IActionResult ListArtistsBySong(int songId)
        {
            List<ArtistViewModel> artistVMs = new List<ArtistViewModel>();

            
            foreach(Artist a in songContainer.GetArtistsBySong(songId))
            {
                artistVMs.Add(new ArtistViewModel(a.ID, a.Name, a.Age));
            }

            Song s = songContainer.GetSongByID(songId);
            SongViewModel songVM = new SongViewModel(s.ID, s.Name, s.Length);

            ArtistsSongViewModel artistsSongVM = new ArtistsSongViewModel(artistVMs, songVM);

            return View(artistsSongVM);
        }

        public IActionResult EditMusic(List<int> ids)
        {
            List<SongViewModel> musicVMs = new List<SongViewModel>();
            foreach (int id in ids)
            {
                Song song = songContainer.GetSongByID(id);
                musicVMs.Add(new SongViewModel(song.ID, song.Name, song.Length));
            }

            return View(musicVMs);
        }

        [HttpPost]
        public IActionResult EditMusic(SongViewModel musicVM)
        {
            Song song = new Song(musicVM.ID, musicVM.Name, musicVM.Length);
            songContainer.UpdateSong(song);
            return RedirectToAction("ListMusic");
        }

        [HttpPost]
        public IActionResult AddMusic(SongViewModel musicVM)
        {
            Song song = new Song(musicVM.Name, musicVM.Length);
            songContainer.InsertSong(song);
            ViewBag.Music = musicVM;
            return RedirectToAction("AddMusic");
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            songContainer.DeleteSongByID(id);
            return Ok(id);
        }

        public IActionResult LinkArtistToSong()
        {
            List<Artist> artists = artistContainer.LoadArtist();
            List<Song> songs = songContainer.GetSongs();
            ArtistsSongsViewModel artistsSongsVM = new ArtistsSongsViewModel(artists[0].ID, artists, songs[0].ID, songs);

            return View(artistsSongsVM);
        }

        [HttpPost]
        public IActionResult LinkArtistToSong(ArtistsSongsViewModel artistSongsVM)
        {
            songContainer.LinkSongToArtist(artistSongsVM.SelectedSongID, artistContainer.FindArtistByID(artistSongsVM.SelectedArtistID));
            return RedirectToAction("ListArtistsbySong", new { songId = artistSongsVM.SelectedSongID });
        }
    }
}
