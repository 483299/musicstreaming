﻿using Microsoft.AspNetCore.Mvc;
using LogicLayer;
using DataAccessLayer;
using PresentationLayer.Models;

namespace PresentationLayer.Controllers
{
    public class ArtistController : Controller
    {
        private readonly IArtistContainer artistContainer;

        public ArtistController(IArtistContainer artistContainer)
        {
            this.artistContainer = artistContainer;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AddArtist()
        {
            return View();
        }

        public IActionResult ListArtist()
        {
            List<ArtistViewModel> artists = new List<ArtistViewModel>();

            foreach(Artist artist in artistContainer.LoadArtist()) {
                artists.Add(new ArtistViewModel(artist.ID, artist.Name, artist.Age));
            }

            return View(artists);
        }

        [HttpPost]
        public IActionResult AddArtist(ArtistViewModel artistViewModel)
        {
            Artist artist = new Artist(artistViewModel.Name, artistViewModel.Age);
            artistContainer.InsertArtist(artist);
            ViewBag.Music = artistViewModel;
            return RedirectToAction("AddArtist");
        }

        public IActionResult ListSongsByArtist(int artistId)
        {
            List<SongViewModel> songs = new List<SongViewModel>();

            foreach (Song song in artistContainer.GetSongsByArtist(artistId))
            {
                songs.Add(new SongViewModel(song.ID, song.Name, song.Length));
            }
            Artist artist = artistContainer.FindArtistByID(artistId);
            ArtistViewModel artistVM = new ArtistViewModel(artist.ID, artist.Name, artist.Age);

            SongsArtistViewModel songsArtistViewModel = new SongsArtistViewModel(songs, artistVM);
            return View(songsArtistViewModel);
        }


        [HttpPost]
        public IActionResult UnlinkSongFromArtist(int artistId, int songId)
        {
            artistContainer.UnlinkArtistFromSong(artistId, songId);
            return RedirectToAction("ListArtistsbySong", "Song", new { songId = songId });
        }
    }
}
