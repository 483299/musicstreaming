﻿namespace PresentationLayer.Models
{
    public class SongViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }


        public SongViewModel(int ID, string Name, int Length)
        {
            this.ID = ID;
            this.Name = Name;
            this.Length = Length;
        }

        public SongViewModel()
        {
            
        }
    }
}
