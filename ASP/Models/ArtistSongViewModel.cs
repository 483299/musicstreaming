﻿using LogicLayer;

namespace PresentationLayer.Models
{
    public class ArtistSongViewModel
    {
        public ArtistViewModel Artist;
        public SongViewModel Song;

        public ArtistSongViewModel(ArtistViewModel Artist, SongViewModel Song)
        {
            this.Artist = Artist;
            this.Song = Song;
        }

        public ArtistSongViewModel()
        {
             
        }
    }
}
