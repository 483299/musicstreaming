﻿namespace PresentationLayer.Models
{
    public class SongsArtistViewModel
    {
        public ArtistViewModel Artist { get; set; }

        public List<SongViewModel> Songs { get; set; }

        public SongsArtistViewModel(List<SongViewModel> Songs, ArtistViewModel Artist)
        {
            this.Artist = Artist;
            this.Songs = Songs;
        }

        public SongsArtistViewModel()
        {
            
        }
    }

}
