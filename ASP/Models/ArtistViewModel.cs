﻿namespace PresentationLayer.Models
{
    public class ArtistViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }


        public ArtistViewModel(int ID, string Name, int Age)
        {
            this.ID = ID;
            this.Name = Name;
            this.Age = Age;
        }

        public ArtistViewModel()
        {

        }
    }
}