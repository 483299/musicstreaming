﻿using LogicLayer;

namespace PresentationLayer.Models
{
    public class ArtistsSongsViewModel
    {
        public int SelectedArtistID { get; set; }
        public List<Artist> Artists { get; set; }
        public int SelectedSongID { get; set; }
        public List<Song> Songs { get; set; }

        public ArtistsSongsViewModel(int SelectedArtistID, List<Artist> Artists, int SelectedSongID, List<Song> Songs)
        {
            this.SelectedArtistID = SelectedArtistID;
            this.Artists = Artists;
            this.SelectedSongID = SelectedSongID;
            this.Songs = Songs;
        }

        public ArtistsSongsViewModel()
        {

        }
    }
}
