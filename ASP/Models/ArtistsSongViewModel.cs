﻿namespace PresentationLayer.Models
{
    public class ArtistsSongViewModel
    {
        public List<ArtistViewModel> Artists { get; set; }
        public SongViewModel Song { get; set; }

        public ArtistsSongViewModel(List<ArtistViewModel> Artists, SongViewModel Song)
        {
            this.Artists = Artists;
            this.Song = Song;
        }

        public ArtistsSongViewModel()
        {

        }
    }

}
