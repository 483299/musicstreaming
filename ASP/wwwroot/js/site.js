﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$("#DeleteButton").on("click", () => {
    let ids = []
    $("input:checkbox[name=musicCheckbox]:checked").each(function () {
        ids.push($(this).val());
    });

    ids.forEach((val) => {
        fetch(`/Song/Delete?id=${val}`, { method: "POST" }).then(() => window.location.reload())
    })
})

$("#EditButton").on("click", () => {
    let url = "/Song/EditMusic";
    $("input:checkbox[name=musicCheckbox]:checked").each(function (index, element) {
        if (index === 0) {
            url += (`?ids=${$(this).val()}`)
        } else {
            url += (`&ids=${$(this).val()}`)
        }
    });
    
    fetch(url).then((res) => {
        return res.text()
    }).then((html) => {
        document.body.innerHTML = html
    })
})