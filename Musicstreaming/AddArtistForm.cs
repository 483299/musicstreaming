﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLayer;
using LogicLayer;

namespace Musicstreaming
{
    public partial class AddArtistForm : Form
    {

        private ArtistContainer artistContainer = new ArtistContainer(new ArtistDAL());

        public AddArtistForm()
        {
            InitializeComponent();
        }

        private void Commitbtn_Click(object sender, EventArgs e)
        {
            Commit();
        }

        private void Commit()
        {
            Artist artist = new Artist(Nametxtbox.Text, int.Parse(Agetxtbox.Text));
            artistContainer.InsertArtist(artist);
        }
    }
}
