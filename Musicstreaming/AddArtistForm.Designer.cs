﻿namespace Musicstreaming
{
    partial class AddArtistForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Nametxtbox = new System.Windows.Forms.TextBox();
            this.Agetxtbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Commitbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "Home";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 61);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 34);
            this.button2.TabIndex = 1;
            this.button2.Text = "Favorite";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Nametxtbox
            // 
            this.Nametxtbox.Location = new System.Drawing.Point(199, 64);
            this.Nametxtbox.Name = "Nametxtbox";
            this.Nametxtbox.Size = new System.Drawing.Size(150, 31);
            this.Nametxtbox.TabIndex = 2;
            // 
            // Agetxtbox
            // 
            this.Agetxtbox.Location = new System.Drawing.Point(401, 64);
            this.Agetxtbox.Name = "Agetxtbox";
            this.Agetxtbox.Size = new System.Drawing.Size(150, 31);
            this.Agetxtbox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(401, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Age";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(199, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Name";
            // 
            // Commitbtn
            // 
            this.Commitbtn.Location = new System.Drawing.Point(439, 142);
            this.Commitbtn.Name = "Commitbtn";
            this.Commitbtn.Size = new System.Drawing.Size(112, 34);
            this.Commitbtn.TabIndex = 6;
            this.Commitbtn.Text = "Commit";
            this.Commitbtn.UseVisualStyleBackColor = true;
            this.Commitbtn.Click += new System.EventHandler(this.Commitbtn_Click);
            // 
            // AddArtistForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Commitbtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Agetxtbox);
            this.Controls.Add(this.Nametxtbox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "AddArtistForm";
            this.Text = "AddArtistForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button1;
        private Button button2;
        private TextBox Nametxtbox;
        private TextBox Agetxtbox;
        private Label label1;
        private Label label2;
        private Button Commitbtn;
    }
}