﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using LogicLayer;
using DataAccessLayer;

namespace Musicstreaming
{
    public partial class DeleteMusicForm : Form
    {
        private SongContainer songContainer = new SongContainer(new SongDAL());

        public DeleteMusicForm()
        {
            InitializeComponent();

            LoadMusic();
        }

        private void Favorietenbtn_Click(object sender, EventArgs e)
        {
            FavorietenPagina Favorieten = new FavorietenPagina();
            Favorieten.Show();
            this.Close();
        }

        private void Homebtn_Click(object sender, EventArgs e)
        {
            HomeForm home = new HomeForm();
            home.Show();
            this.Close();
        }

        private void DeleteMusicForm_Load(object sender, EventArgs e)
        {
            LoadMusic();
        }

        private void LoadMusic()
        {
            listviewbox.Items.Clear();

            songContainer.GetSongs().ForEach(song =>
            {
                ListViewItem item = new(song.ID.ToString());
                item.SubItems.Add(song.Name);
                item.SubItems.Add(song.Length.ToString());
                listviewbox.Items.Add(item);
            });
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listviewbox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
            //songContainer.DeleteSongByID(listviewbox.SelectedItems[0]);
        }
    }
}
