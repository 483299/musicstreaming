﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Musicstreaming
{
    public partial class ZoekresultaatForm : Form
    {
        public ZoekresultaatForm()
        {
            InitializeComponent();
        }

        private void Homebtn_Click(object sender, EventArgs e)
        {
            HomeForm f1 = new HomeForm();
            f1.Show();
            this.Close();
        }

        private void Favorietenbtn_Click(object sender, EventArgs e)
        {
            FavorietenPagina Fp = new FavorietenPagina();
            Fp.Show();
            this.Close();
        }
    }
}
