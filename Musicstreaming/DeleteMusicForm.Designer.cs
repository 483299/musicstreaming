﻿namespace Musicstreaming
{
    partial class DeleteMusicForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Homebtn = new System.Windows.Forms.Button();
            this.Favorietenbtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Deletebtn = new System.Windows.Forms.Button();
            this.songBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ColumnID = new System.Windows.Forms.ColumnHeader();
            this.ColumnName = new System.Windows.Forms.ColumnHeader();
            this.ColumnLength = new System.Windows.Forms.ColumnHeader();
            this.listviewbox = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.songBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Homebtn
            // 
            this.Homebtn.Location = new System.Drawing.Point(12, 12);
            this.Homebtn.Name = "Homebtn";
            this.Homebtn.Size = new System.Drawing.Size(112, 34);
            this.Homebtn.TabIndex = 0;
            this.Homebtn.Text = "Home";
            this.Homebtn.UseVisualStyleBackColor = true;
            this.Homebtn.Click += new System.EventHandler(this.Homebtn_Click);
            // 
            // Favorietenbtn
            // 
            this.Favorietenbtn.Location = new System.Drawing.Point(12, 65);
            this.Favorietenbtn.Name = "Favorietenbtn";
            this.Favorietenbtn.Size = new System.Drawing.Size(112, 34);
            this.Favorietenbtn.TabIndex = 1;
            this.Favorietenbtn.Text = "Favorieten";
            this.Favorietenbtn.UseVisualStyleBackColor = true;
            this.Favorietenbtn.Click += new System.EventHandler(this.Favorietenbtn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(638, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 31);
            this.textBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(374, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 3;
            // 
            // Deletebtn
            // 
            this.Deletebtn.Location = new System.Drawing.Point(529, 404);
            this.Deletebtn.Name = "Deletebtn";
            this.Deletebtn.Size = new System.Drawing.Size(112, 34);
            this.Deletebtn.TabIndex = 5;
            this.Deletebtn.Text = "Delete";
            this.Deletebtn.UseVisualStyleBackColor = true;
            this.Deletebtn.Click += new System.EventHandler(this.Deletebtn_Click);
            // 
            // ColumnID
            // 
            this.ColumnID.Text = "ID";
            // 
            // ColumnName
            // 
            this.ColumnName.Text = "Name";
            // 
            // ColumnLength
            // 
            this.ColumnLength.Text = "Length";
            // 
            // listviewbox
            // 
            this.listviewbox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnID,
            this.ColumnName,
            this.ColumnLength});
            this.listviewbox.Location = new System.Drawing.Point(174, 65);
            this.listviewbox.Name = "listviewbox";
            this.listviewbox.Size = new System.Drawing.Size(481, 312);
            this.listviewbox.TabIndex = 4;
            this.listviewbox.UseCompatibleStateImageBehavior = false;
            this.listviewbox.View = System.Windows.Forms.View.Details;
            this.listviewbox.SelectedIndexChanged += new System.EventHandler(this.listviewbox_SelectedIndexChanged);
            // 
            // DeleteMusicForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Deletebtn);
            this.Controls.Add(this.listviewbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Favorietenbtn);
            this.Controls.Add(this.Homebtn);
            this.Name = "DeleteMusicForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DeleteMusicForm";
            this.Load += new System.EventHandler(this.DeleteMusicForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.songBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button Homebtn;
        private Button Favorietenbtn;
        private TextBox textBox1;
        private Label label1;
        private Button Deletebtn;
        private ListView listviewbox;
        private BindingSource songBindingSource;
        private ColumnHeader ColumnID;
        private ColumnHeader ColumnName;
        private ColumnHeader ColumnLength;
    }
}