﻿namespace Musicstreaming
{
    partial class AddMusicForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Homebtn = new System.Windows.Forms.Button();
            this.Favorietenbtn = new System.Windows.Forms.Button();
            this.Nametxtbox = new System.Windows.Forms.TextBox();
            this.Lengthtextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.IDtxtbox = new System.Windows.Forms.TextBox();
            this.Commitbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Homebtn
            // 
            this.Homebtn.Location = new System.Drawing.Point(12, 12);
            this.Homebtn.Name = "Homebtn";
            this.Homebtn.Size = new System.Drawing.Size(112, 34);
            this.Homebtn.TabIndex = 0;
            this.Homebtn.Text = "Home";
            this.Homebtn.UseVisualStyleBackColor = true;
            this.Homebtn.Click += new System.EventHandler(this.Homebtn_Click);
            // 
            // Favorietenbtn
            // 
            this.Favorietenbtn.Location = new System.Drawing.Point(12, 62);
            this.Favorietenbtn.Name = "Favorietenbtn";
            this.Favorietenbtn.Size = new System.Drawing.Size(112, 34);
            this.Favorietenbtn.TabIndex = 1;
            this.Favorietenbtn.Text = "Favorieten";
            this.Favorietenbtn.UseVisualStyleBackColor = true;
            this.Favorietenbtn.Click += new System.EventHandler(this.Favorietenbtn_Click);
            // 
            // Nametxtbox
            // 
            this.Nametxtbox.Location = new System.Drawing.Point(173, 62);
            this.Nametxtbox.Name = "Nametxtbox";
            this.Nametxtbox.Size = new System.Drawing.Size(150, 31);
            this.Nametxtbox.TabIndex = 2;
            // 
            // Lengthtextbox
            // 
            this.Lengthtextbox.Location = new System.Drawing.Point(354, 62);
            this.Lengthtextbox.Name = "Lengthtextbox";
            this.Lengthtextbox.Size = new System.Drawing.Size(150, 31);
            this.Lengthtextbox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(391, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Length";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(586, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "ID";
            // 
            // IDtxtbox
            // 
            this.IDtxtbox.Location = new System.Drawing.Point(524, 62);
            this.IDtxtbox.Name = "IDtxtbox";
            this.IDtxtbox.Size = new System.Drawing.Size(150, 31);
            this.IDtxtbox.TabIndex = 8;
            // 
            // Commitbtn
            // 
            this.Commitbtn.Location = new System.Drawing.Point(354, 209);
            this.Commitbtn.Name = "Commitbtn";
            this.Commitbtn.Size = new System.Drawing.Size(112, 34);
            this.Commitbtn.TabIndex = 6;
            this.Commitbtn.Text = "Commit";
            this.Commitbtn.UseVisualStyleBackColor = true;
            this.Commitbtn.Click += new System.EventHandler(this.Commitbtn_Click);
            // 
            // AddMusicForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.IDtxtbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Commitbtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Lengthtextbox);
            this.Controls.Add(this.Nametxtbox);
            this.Controls.Add(this.Favorietenbtn);
            this.Controls.Add(this.Homebtn);
            this.Name = "AddMusicForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddMusicForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button Homebtn;
        private Button Favorietenbtn;
        private TextBox Nametxtbox;
        private TextBox Lengthtextbox;
        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox IDtxtbox;
        private Button Commitbtn;
    }
}