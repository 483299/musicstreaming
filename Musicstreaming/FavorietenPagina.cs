﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Musicstreaming
{
    public partial class FavorietenPagina : Form
    {

        MusicPlayer musicPlayer = new MusicPlayer();

        public FavorietenPagina()
        {
            InitializeComponent();
        }

        private void Homebtn_Click(object sender, EventArgs e)
        {
            HomeForm f1 = new HomeForm();
            f1.Show();
            this.Close();
        }

        private void Filebtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "MP3 files|*.mp3";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    musicPlayer.open(ofd.FileName);
                }
            }
        }

        private void Playbtn_Click(object sender, EventArgs e)
        {
            musicPlayer.play();
        }

        private void Pausebtn_Click(object sender, EventArgs e)
        {
            musicPlayer.stop();
        }
    }
}
