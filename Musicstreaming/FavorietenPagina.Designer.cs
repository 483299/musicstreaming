﻿namespace Musicstreaming
{
    partial class FavorietenPagina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Homebtn = new System.Windows.Forms.Button();
            this.Favorietenbtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Playbtn = new System.Windows.Forms.Button();
            this.Repeatbtn = new System.Windows.Forms.Button();
            this.Fastforwardbtn = new System.Windows.Forms.Button();
            this.Pausebtn = new System.Windows.Forms.Button();
            this.Filebtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Homebtn
            // 
            this.Homebtn.Location = new System.Drawing.Point(12, 12);
            this.Homebtn.Name = "Homebtn";
            this.Homebtn.Size = new System.Drawing.Size(112, 34);
            this.Homebtn.TabIndex = 0;
            this.Homebtn.Text = "Home";
            this.Homebtn.UseVisualStyleBackColor = true;
            this.Homebtn.Click += new System.EventHandler(this.Homebtn_Click);
            // 
            // Favorietenbtn
            // 
            this.Favorietenbtn.Location = new System.Drawing.Point(12, 78);
            this.Favorietenbtn.Name = "Favorietenbtn";
            this.Favorietenbtn.Size = new System.Drawing.Size(112, 34);
            this.Favorietenbtn.TabIndex = 1;
            this.Favorietenbtn.Text = "Favorieten";
            this.Favorietenbtn.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(638, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 31);
            this.textBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(342, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Favorieten";
            // 
            // Playbtn
            // 
            this.Playbtn.Location = new System.Drawing.Point(320, 348);
            this.Playbtn.Name = "Playbtn";
            this.Playbtn.Size = new System.Drawing.Size(133, 90);
            this.Playbtn.TabIndex = 4;
            this.Playbtn.Text = "PLAY";
            this.Playbtn.UseVisualStyleBackColor = true;
            this.Playbtn.Click += new System.EventHandler(this.Playbtn_Click);
            // 
            // Repeatbtn
            // 
            this.Repeatbtn.Location = new System.Drawing.Point(159, 348);
            this.Repeatbtn.Name = "Repeatbtn";
            this.Repeatbtn.Size = new System.Drawing.Size(133, 90);
            this.Repeatbtn.TabIndex = 5;
            this.Repeatbtn.Text = "REPEAT";
            this.Repeatbtn.UseVisualStyleBackColor = true;
            // 
            // Fastforwardbtn
            // 
            this.Fastforwardbtn.Location = new System.Drawing.Point(484, 348);
            this.Fastforwardbtn.Name = "Fastforwardbtn";
            this.Fastforwardbtn.Size = new System.Drawing.Size(143, 90);
            this.Fastforwardbtn.TabIndex = 6;
            this.Fastforwardbtn.Text = "FASTFOWARD";
            this.Fastforwardbtn.UseVisualStyleBackColor = true;
            // 
            // Pausebtn
            // 
            this.Pausebtn.Location = new System.Drawing.Point(655, 348);
            this.Pausebtn.Name = "Pausebtn";
            this.Pausebtn.Size = new System.Drawing.Size(133, 90);
            this.Pausebtn.TabIndex = 7;
            this.Pausebtn.Text = "PAUSE";
            this.Pausebtn.UseVisualStyleBackColor = true;
            this.Pausebtn.Click += new System.EventHandler(this.Pausebtn_Click);
            // 
            // Filebtn
            // 
            this.Filebtn.Location = new System.Drawing.Point(12, 348);
            this.Filebtn.Name = "Filebtn";
            this.Filebtn.Size = new System.Drawing.Size(133, 90);
            this.Filebtn.TabIndex = 8;
            this.Filebtn.Text = "FILE";
            this.Filebtn.UseVisualStyleBackColor = true;
            this.Filebtn.Click += new System.EventHandler(this.Filebtn_Click);
            // 
            // FavorietenPagina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Filebtn);
            this.Controls.Add(this.Pausebtn);
            this.Controls.Add(this.Fastforwardbtn);
            this.Controls.Add(this.Repeatbtn);
            this.Controls.Add(this.Playbtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Favorietenbtn);
            this.Controls.Add(this.Homebtn);
            this.Name = "FavorietenPagina";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FavorietenPagina";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button Homebtn;
        private Button Favorietenbtn;
        private TextBox textBox1;
        private Label label1;
        private Button Playbtn;
        private Button Repeatbtn;
        private Button Fastforwardbtn;
        private Button Pausebtn;
        private Button Filebtn;
    }
}