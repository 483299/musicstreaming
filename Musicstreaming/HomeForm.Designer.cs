﻿namespace Musicstreaming
{
    partial class HomeForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Homebtn = new System.Windows.Forms.Button();
            this.Favorietenbtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.AddMusicbtn = new System.Windows.Forms.Button();
            this.Deletebtn = new System.Windows.Forms.Button();
            this.AddArtisttxtbox = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Homebtn
            // 
            this.Homebtn.Location = new System.Drawing.Point(12, 12);
            this.Homebtn.Name = "Homebtn";
            this.Homebtn.Size = new System.Drawing.Size(112, 34);
            this.Homebtn.TabIndex = 0;
            this.Homebtn.Text = "Home";
            this.Homebtn.UseVisualStyleBackColor = true;
            this.Homebtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // Favorietenbtn
            // 
            this.Favorietenbtn.Location = new System.Drawing.Point(12, 75);
            this.Favorietenbtn.Name = "Favorietenbtn";
            this.Favorietenbtn.Size = new System.Drawing.Size(112, 34);
            this.Favorietenbtn.TabIndex = 1;
            this.Favorietenbtn.Text = "Favorites";
            this.Favorietenbtn.UseVisualStyleBackColor = true;
            this.Favorietenbtn.Click += new System.EventHandler(this.Favorietenbtn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(638, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 31);
            this.textBox1.TabIndex = 2;
            this.textBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox1_MouseClick);
            // 
            // AddMusicbtn
            // 
            this.AddMusicbtn.Location = new System.Drawing.Point(12, 136);
            this.AddMusicbtn.Name = "AddMusicbtn";
            this.AddMusicbtn.Size = new System.Drawing.Size(112, 34);
            this.AddMusicbtn.TabIndex = 3;
            this.AddMusicbtn.Text = "Add Music";
            this.AddMusicbtn.UseVisualStyleBackColor = true;
            this.AddMusicbtn.Click += new System.EventHandler(this.AddMusicbtn_Click);
            // 
            // Deletebtn
            // 
            this.Deletebtn.Location = new System.Drawing.Point(12, 189);
            this.Deletebtn.Name = "Deletebtn";
            this.Deletebtn.Size = new System.Drawing.Size(112, 34);
            this.Deletebtn.TabIndex = 4;
            this.Deletebtn.Text = "Delete Music";
            this.Deletebtn.UseVisualStyleBackColor = true;
            this.Deletebtn.Click += new System.EventHandler(this.Deletebtn_Click);
            // 
            // AddArtisttxtbox
            // 
            this.AddArtisttxtbox.Location = new System.Drawing.Point(12, 239);
            this.AddArtisttxtbox.Name = "AddArtisttxtbox";
            this.AddArtisttxtbox.Size = new System.Drawing.Size(112, 34);
            this.AddArtisttxtbox.TabIndex = 5;
            this.AddArtisttxtbox.Text = "Add Artist";
            this.AddArtisttxtbox.UseVisualStyleBackColor = true;
            this.AddArtisttxtbox.Click += new System.EventHandler(this.AddArtisttxtbox_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.AddArtisttxtbox);
            this.Controls.Add(this.Deletebtn);
            this.Controls.Add(this.AddMusicbtn);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Favorietenbtn);
            this.Controls.Add(this.Homebtn);
            this.Name = "HomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button Homebtn;
        private Button Favorietenbtn;
        private TextBox textBox1;
        private Button AddMusicbtn;
        private Button Deletebtn;
        private Button AddArtisttxtbox;
    }
}