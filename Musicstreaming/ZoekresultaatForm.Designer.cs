﻿namespace Musicstreaming
{
    partial class ZoekresultaatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Homebtn = new System.Windows.Forms.Button();
            this.Favorietenbtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Resultatenlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Homebtn
            // 
            this.Homebtn.Location = new System.Drawing.Point(12, 12);
            this.Homebtn.Name = "Homebtn";
            this.Homebtn.Size = new System.Drawing.Size(112, 34);
            this.Homebtn.TabIndex = 0;
            this.Homebtn.Text = "Home";
            this.Homebtn.UseVisualStyleBackColor = true;
            this.Homebtn.Click += new System.EventHandler(this.Homebtn_Click);
            // 
            // Favorietenbtn
            // 
            this.Favorietenbtn.Location = new System.Drawing.Point(12, 77);
            this.Favorietenbtn.Name = "Favorietenbtn";
            this.Favorietenbtn.Size = new System.Drawing.Size(112, 34);
            this.Favorietenbtn.TabIndex = 1;
            this.Favorietenbtn.Text = "Favorieten";
            this.Favorietenbtn.UseVisualStyleBackColor = true;
            this.Favorietenbtn.Click += new System.EventHandler(this.Favorietenbtn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(638, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 31);
            this.textBox1.TabIndex = 2;
            // 
            // Resultatenlabel
            // 
            this.Resultatenlabel.AutoSize = true;
            this.Resultatenlabel.Location = new System.Drawing.Point(355, 21);
            this.Resultatenlabel.Name = "Resultatenlabel";
            this.Resultatenlabel.Size = new System.Drawing.Size(93, 25);
            this.Resultatenlabel.TabIndex = 3;
            this.Resultatenlabel.Text = "Resultaten";
            // 
            // ZoekresultaatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Resultatenlabel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Favorietenbtn);
            this.Controls.Add(this.Homebtn);
            this.Name = "ZoekresultaatForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZoekresultaatForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button Homebtn;
        private Button Favorietenbtn;
        private TextBox textBox1;
        private Label Resultatenlabel;
    }
}