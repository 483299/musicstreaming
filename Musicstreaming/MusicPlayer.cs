﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Media;
using System.Runtime.InteropServices;
using NAudio.Wave;

namespace Musicstreaming
{
    class MusicPlayer
    {
        WaveOutEvent output = new WaveOutEvent();
        AudioFileReader? currentAudioFile;

        public void open(string file)
        {
            currentAudioFile = new AudioFileReader(file);
        }

        public void play()
        {
            if(currentAudioFile == null) { return; }

            output.Init(currentAudioFile);
            output.Play();
        }

        public void stop()
        {
            output.Stop();
        }
    }
}
