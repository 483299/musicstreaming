namespace Musicstreaming
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HomeForm f1 = new HomeForm();
            f1.Show();
            this.Close();
        }

        private void Favorietenbtn_Click(object sender, EventArgs e)
        {
            FavorietenPagina f2 = new FavorietenPagina();
            this.Hide();
            f2.Show();
        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            ZoekresultaatForm Zf = new ZoekresultaatForm();
            this.Hide();
            Zf.Show();
        }

        private void AddMusicbtn_Click(object sender, EventArgs e)
        {
            AddMusicForm Amf = new AddMusicForm();
            this.Hide();
            Amf.Show();
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
            DeleteMusicForm Dmf = new DeleteMusicForm();
            this.Hide();
            Dmf.Show();
        }

        private void AddArtisttxtbox_Click(object sender, EventArgs e)
        {
            AddArtistForm Adf = new AddArtistForm();
            this.Hide();
            Adf.Show();
        }
    }
}