﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using LogicLayer;
using DataAccessLayer;

namespace Musicstreaming
{
    public partial class AddMusicForm : Form
    {
        private SongContainer songContainer = new SongContainer(new SongDAL());

        public AddMusicForm()
        {
            InitializeComponent();
        }


        private void Commitbtn_Click(object sender, EventArgs e)
        {
            NewSong();
        }

        private void Favorietenbtn_Click(object sender, EventArgs e)
        {
            FavorietenPagina Fp = new FavorietenPagina();
            Fp.Show();
            this.Close();
        }

        private void Homebtn_Click(object sender, EventArgs e)
        {
            HomeForm Hf = new HomeForm();
            Hf.Show();
            this.Close();
        }

        private void NewSong()
        {
            Song song = new Song(Nametxtbox.Text, int.Parse(Lengthtextbox.Text));
            songContainer.InsertSong(song);
        }
    }
}
